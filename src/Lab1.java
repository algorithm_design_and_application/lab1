public class Lab1 {
	static int n;

	public static void f1() {
		long sum = 0;
		for (int i = 1; i <= n; i++)
			sum++;
		System.out.println("sum = " + sum);
	}

	public static void f2() {
		long sum = 0;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				sum++;
		System.out.println("sum = " + sum);
	}

	public static void f3() {
		long sum = 0;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= i; j++)
				sum++;
		System.out.println("sum = " + sum);
	}

	public static void f4() {
		long sum = 0;
		for (int k = 1; k <= n; k *= 2)
			for (int j = 1; j <= n; j++)
				sum++;
		System.out.println("sum = " + sum);
	}

	public static void f5() {
		long sum = 0;
		for (int k = 1; k <= n; k *= 2)
			for (int j = 1; j <= k; j++)
				sum++;
		System.out.println("sum = " + sum);
	}

	public static void main(String[] args) {
		long start, stop;

		n = 500000; // try to change this value

		start = System.nanoTime();

		f1(); // try to call other methods

		stop = System.nanoTime();
		System.out.println("time = " + (stop - start) + " nano secs.");
	}

}